
<p align="center">
<a href="https://github.com/nhrrob/laravel-8-api-crud/issues"><img alt="GitHub issues" src="https://img.shields.io/github/issues/nhrrob/laravel-8-api-crud"></a>
<a href="https://github.com/nhrrob/laravel-8-api-crud/network"><img alt="GitHub forks" src="https://img.shields.io/github/forks/nhrrob/laravel-8-api-crud"></a>
<a href="https://github.com/nhrrob/laravel-8-api-crud/stargazers"><img alt="GitHub stars" src="https://img.shields.io/github/stars/nhrrob/laravel-8-api-crud"></a>
<a href="https://github.com/nhrrob/laravel-8-api-crud/blob/master/LICENSE.md"><img alt="GitHub license" src="https://img.shields.io/github/license/nhrrob/laravel-8-api-crud"></a>

</p>

SETUP CRONJOB SCHEDULER DAILY CRAWLING
read articles from https://laravelarticle.com/laravel-scheduler-on-cpanel-shared-hosting

## Laravel 8 API Crud (Passport)

Laravel 8 API Crud is a basic RESTful API crud app built with Laravel 8 and Passport. In this project a rest api created for managing product crud operations. 

Features (API) include:

- Laravel passport package
- Authentication using passportn
- Logout to remove old tokens 
- Show Movie.
- Crawling Manual.
- Search Movie.
- Random Show Movie
- Pagination link with json data



## Install

Install commands:
``` 
- composer update
- add .env and update database settings
- php artisan migrate:fresh --seed
- php artisan passport:install
- php artisan serve
- 

```

Use Postman to test the API.


## Note

- Login: 
    - URL: http://laravel-8-api-crud.rob/api/login 
    - Method: POST
    - Insert email and password: Body tab => x-www-form-urlencode
    - Press Enter to get Bearer token;
    - For future request add this token: 
      <br>Authorization tab: Type => Bearer Token; Insert token.
    
- Insert/Update:
    - Use Body tab => x-www-form-urlencode : Add title key and its value
    - Another way: Body tab => raw : select json type 
- Demo User (database/seeders/DatabaseSeeder.php): 
<br> ```admin@admin.com/password```


## License

The Laravel 8 Crud is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


## Contact

