<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Models\Movie;
use App\Models\Crawl;

class DailyCrawl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:crawl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawling data film daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lastCount = Crawl::where('id', '=','1')->sum('last_index_count');
        $numb = $lastCount;
        $client = new \GuzzleHttp\Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $request = $client->post($url,  
                            [
                                'headers' => [ 'Authorization' => 'Bearer '.$token  ],
                                'body'=> json_encode(
                                ['StartIndex' => $lastCount]
                            )],
                        );

        $response = collect(json_decode($request->getBody(), true));
        $getCounting = $response['count'];
        $response = (array)$response['data'];
        
        // $response = json_decode($request->getBody(), true);
        // $response = (array)$response;
        // $response =$response['data'];
        echo "<pre>";
        // print_r($response);
        foreach ($response as $datas) {
            $datas = (array)$datas;
            echo "<pre>";
            print_r($datas['title_en']);
            echo "success added to database";
                Movie::updateOrCreate(
                ['id'=>$datas['id']],
                [
                'id'=>$datas['id'],
                'title_th'=>$datas['title_th'],
                'tags_th'=>$datas['tags_th'],
                'description_th'=>$datas['description_th'],
                'releaseDate'=>$datas['releaseDate'],
                'language'=>$datas['language'],
                'actor'=>$datas['actor'],
                'duration'=>$datas['duration'],
                'cover'=>$datas['cover'],
                'status'=>$datas['status'],
                'sysCreateTime'=>$datas['sysCreateTime'],
                'contentUrl'=>$datas['contentUrl']]
            );   
        }
        $totalCount = $getCounting + $numb ;
        Crawl::where('id','=','1')->update(['last_index_count'=>$totalCount]);
        echo "CRAWLING ENDED, CRAWLING SUCCESSFUL";
        \Log::info("Cron is working fine!");
    }
}
