<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Crawl;
use Illuminate\Support\Str;
use App\Http\Resources\MovieResource;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class MovieController extends Controller
{
    //
    // public function movies() {
    //     $products = Product::latest()->paginate(10);
    //     return ProductResource::collection($products);
    // }

    public function index () 
    {

        $lastCount = Crawl::where('id', '=','1')->sum('last_index_count');
        // $countNumber = (int)$lastCount;
        $numb = $lastCount;
        echo $numb. "ini nm" ;
        $client = new \GuzzleHttp\Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $url = env('API_BASE_URL');
        $token = env('API_TOKEN');
        $myBody = 40;
        $request = $client->post($url,  
                            [
                                'headers' => [ 'Authorization' => 'Bearer '.$token  ],
                                'body'=> json_encode(
                                ['StartIndex' => $lastCount]
                            )],
                        );

        $response = collect(json_decode($request->getBody(), true));
        $getCounting = $response['count'];
        echo $getCounting. "ini count";
        $response = (array)$response['data'];
        
        // $response = json_decode($request->getBody(), true);
        // $response = (array)$response;
        // $response =$response['data'];
        echo "<pre>";
        // print_r($response);
        foreach ($response as $datas) {
            $datas = (array)$datas;
            echo "<pre>";
            print_r($datas['title_en']);
            Movie::updateOrCreate(
            ['id'=>$datas['id']],
            [
            'id'=>$datas['id'],
            'title_th'=>$datas['title_th'],
            'tags_th'=>$datas['tags_th'],
            'description_th'=>$datas['description_th'],
            'releaseDate'=>$datas['releaseDate'],
            'language'=>$datas['language'],
            'actor'=>$datas['actor'],
            'duration'=>$datas['duration'],
            'cover'=>$datas['cover'],
            'status'=>$datas['status'],
            'sysCreateTime'=>$datas['sysCreateTime'],
            'contentUrl'=>$datas['contentUrl']]
        );
            
        }
        

        // Movie::insert(@$response['data']); 
        $totalCount = $getCounting + $numb ;
        Crawl::where('id','=','1')->update(['last_index_count'=>$totalCount]);
        echo 'mantap berhasil' ;
    }
    public function indexPage() {
        $movies = Movie::latest()->paginate(20);
        return $movies;
        // return MovieResource::collection($movies);
    }
    public function randomIndex() {
        $randos = Movie::get()->random(12);
        return $randos;
        // return MovieResource::collection($movies);
    }

    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        return $movie;
        // return MovieResource::collection($movie);
        
    }
    public function search($title_th)
    {
        $movies = Movie::where('title_th', 'like', '%'.$title_th.'%')->get();
        return $movies;
    }
}
