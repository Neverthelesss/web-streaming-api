<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title_th' => $this->title_th,
            'actor' => $this->actor,
            'cover' => $this->cover,
            'duration' => $this->duration,
            'releaseDate' => $this->releaseDate,
            'contentUrl' => $this->contentUrl,
        ];
    }
}
