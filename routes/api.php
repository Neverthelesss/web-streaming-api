<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\MovieController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:api']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/crawls', [MovieController::class,'index'])->name('manual.crawls');
    Route::get('/movies', [MovieController::class,'indexPage'])->name('movies');
    Route::get('/random-movies', [MovieController::class,'randomIndex'])->name('random.movies');
    Route::get('/movies/search/{title_th}', [MovieController::class, 'search']);
    Route::get('/detail-movie/{title_th}', [MovieController::class,'show'] )->name('show.movie');
    Route::get('/movies/{movies}', [MovieController::class,'show'])->name('detailmovies');
    // Route::get('/products/search/{title}', [ProductController::class, 'search']);
});
// Route::apiResource('products', ProductController::class)->middleware('auth:api');